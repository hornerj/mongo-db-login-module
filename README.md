# MongoDB Login-Modul
JBoss Login Modul um die Authentifizierung eines Benutzers mit einer MongoDB-Datenbank zu ermöglichen.
## Konfiguration von Wildfly
Um das Login-Modul nutzen zu können müssen mehrere Änderungen vorgenommen werden. Als erstes muss ein Build des Projekts mit dem Befehl

    mvn clean package

erstellt werden. Im Wildfly-Hauptordner muss folgende Ordnerstruktur erstellt bzw. erweitert werden

    modules/mongologin/main

In den *main*-Ordner müssen folgende Dateien kopiert werden:

- mongo-db-login-module-0.0.1.jar. Diese befindet sich im target-Ordner des Projekts.
- mongo-java-driver-3.12.6.jar. Diese befindet sich im libs-Verzeichnis des Projekts.
- module.xml. Diese befindet sich im etc-Ordner des Projekts.

### Einstellen der Datenbank und der Sammlung
Um das Login-Modul nutzen zu können müssen in der Konfigurationsdatei standalone.xml von Wildfly Änderungen vorgenommen werden. 

Im Tag &lt;subsystem xmlns="urn:jboss:domain:security:2.0"&gt; muss innerhalb des Tags &lt;security-domains&gt; eine neue security-domain eingefügt werden. Unten stehender Code zeigt die Konfiguration für eine MongoDB-Datenbank mit dem Namen my-aktion und der Sammlung mit dem Namen Organizer.

    <security-domain name="mongo-auth" cache-type="default">
        <authentication>
            <login-module 
                    code="de.dpunkt.myaktion.login.MongoDBLoginModule" 
                    flag="required" 
                    module="mongologin">
                <module-option name="database" value="my-aktion"/>
                <module-option name="collection" value="Organizer"/>
            </login-module>
        </authentication>
    </security-domain>

Die Datenbank und die Sammlung in welcher die Nutzer gespeichert sind können hier konfiguriert werden. Der Name der Datenbank sowie der Name der Sammlung können dabei über den Tag module-option übergeben werden.

Um das Login-Modul in einer Anwendung zu nutzen muss in der Konfigurationsdatei jboss-web.xml der Anwendung folgende Referenz auf das eben in der standalone.xml eingetragene Login-Modul erstellt werden.

	<jboss-web>
	    <security-domain>mongo-auth</security-domain>
	</jboss-web>


