package de.dpunkt.myaktion.login;

import java.security.acl.Group;
import java.util.Map;

import javax.security.auth.Subject;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.login.LoginException;

import org.bson.Document;
import org.jboss.security.SimpleGroup;
import org.jboss.security.SimplePrincipal;
import org.jboss.security.auth.spi.UsernamePasswordLoginModule;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;

/**
 * @author Julian
 */
public class MongoDBLoginModule extends UsernamePasswordLoginModule {
    //----------------------------------------------------------------------------------------------

    private String database;

    private String collection;

    //----------------------------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize(Subject subject, CallbackHandler callbackHandler,
            Map<String, ?> sharedState, Map<String, ?> options) {
        super.initialize(subject, callbackHandler, sharedState, options);

        database = (String) options.get("database");
        collection = (String) options.get("collection");
    }

    //----------------------------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getUsersPassword() throws LoginException {
        MongoClient mongoClient = MongoClients.create("mongodb://localhost");
        
        MongoDatabase database = mongoClient.getDatabase(this.database);
        MongoCollection<Document> organizerCollection = database.getCollection(this.collection);
        Document organizerDocument =
                organizerCollection.find(Filters.eq("email", getUsername())).first();

        String password =
                (organizerDocument == null) ? "" : (String) organizerDocument.get("password");

        mongoClient.close();

        return password;
    }

    //----------------------------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean validatePassword(String inputPassword, String expectedPassword) {
        return inputPassword.equals(expectedPassword);
    }

    //----------------------------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    protected Group[] getRoleSets() throws LoginException {
        SimpleGroup group = new SimpleGroup("Roles");
        group.addMember(new SimplePrincipal("Organizer"));

        return new Group[] { group };
    }

    //----------------------------------------------------------------------------------------------
}